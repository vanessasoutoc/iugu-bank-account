Rails.application.routes.draw do
  root to: 'home#index'
  devise_for :users, controllers: {
    sessions: 'users/sessions',
    registrations: 'users/registrations'
  }
  resources :home, only: :index
  resources :transactions

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
