class Users::RegistrationsController < Devise::RegistrationsController
  def create
    super do |created_user|
      if created_user.id
        account = Account.new(user_id: created_user.id,
                              account: rand(8**8).to_s.rjust(8, '0'),
                              agency: 0001, amount: 1000)
        account.save
      end
    end
  end
end
