class HomeController < ApplicationController
  before_action :authenticate_user!

  def index
    @account = current_user.account.first
  end
end
