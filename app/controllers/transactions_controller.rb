class TransactionsController < ApplicationController
  before_action :authenticate_user!
  before_action :set_transaction, only: %i[show]

  def index; end

  def new
    @transaction = Transaction.new
  end

  def show; end

  # POST /transactions
  # POST /transactions.json
  def create
    @transaction = Transaction.new(reply_transaction_params(transaction_params))
    respond_to do |format|
      if @transaction.save
        render_success(format, 'controllers.transaction.messages.create')
      else
        render_fail(format)
      end
    end
  end

  private

  def reply_transaction_params(transaction_params)
    checked_account(transaction_params)
    transaction_params[:amount].delete! '.' if transaction_params[:amount].include? '.'
    money = transaction_params[:amount].sub!(',', '.')
    transaction_params.merge(amount: money.to_f)
  end

  def checked_account(transaction_params)
    return unless transaction_params[:destination_account].present?

    account = Account.where(account: transaction_params[:destination_account])
    transaction_params[:destination_account] = (account[0] if account.count >= 1)
    transaction_params[:source_account] = current_user.account[0]
  end

  def render_success(format, controller)
    format.html do
      redirect_to @transaction
      flash[:success] = I18n.t controller
    end
    format.json { render :show, status: :created, location: @transaction }
  end

  def render_fail(format)
    format.html do
      flash[:error] = @transaction.errors.full_messages[0]
      render :new
    end
    format.json do
      flash[:error] = :unprocessable_entity
      render json: @transaction.errors
    end
  end

  def set_transaction
    @transaction = Transaction.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def transaction_params
    params.require(:transaction).permit(:source_account, :destination_account, :amount)
  end
end
