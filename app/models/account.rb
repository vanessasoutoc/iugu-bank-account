class Account < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  validates_presence_of :agency, :user_id, :amount
  validates :account, uniqueness: true

  belongs_to :user
  has_many :transactions, class_name: 'Transaction', foreign_key: 'source_account'
  has_many :transactions, class_name: 'Transaction', foreign_key: 'destination_account'
end
