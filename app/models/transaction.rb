class Transaction < ApplicationRecord
  after_save :update_balance_account

  validates :amount, presence: true

  belongs_to :source_account, class_name: 'Account'
  belongs_to :destination_account, class_name: 'Account'
  validates :destination_account, presence: true
  validates :source_account, presence: true

  def update_balance_account
    source_account.amount -= amount
    destination_account.amount += amount
    source_account.save
    destination_account.save
  end
end
