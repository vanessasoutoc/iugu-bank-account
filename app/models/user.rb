class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  validates_presence_of :email, if: :email_required?

  validates_presence_of :password, if: :password_required?
  validates_confirmation_of :password, if: :password_required?

  has_many :account

  protected

  def email_required?
    true
  end

  def password_required?
    !password.nil? || !password_confirmation.nil?
  end
end
