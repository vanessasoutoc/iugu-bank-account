class CreateTransactions < ActiveRecord::Migration[6.0]
  def change
    create_table :transactions do |t|
      t.bigint :source_account, null: false
      t.bigint :destination_account, null: false
      t.decimal :amount, { precision: 8, scale: 2 }
    end
  end
end
