class CreateAccounts < ActiveRecord::Migration[6.0]
  def change
    create_table :accounts do |t|
      t.references :user, null: false, foreign_key: true
      t.decimal :amount, {precision: 8, scale: 2}
      t.integer :agency
      t.bigint :account
    end
  end
end
