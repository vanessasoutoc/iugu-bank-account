# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

users = [{ email: 'loja01@gmail.com', password: '@loja0123' },
         { email: 'loja02@gmail.com', password: '@loja0223' },
         { email: 'loja03@gmail.com', password: '@loja0323' },
         { email: 'loja04@gmail.com', password: '@loja0423' }]
accounts = [{ agency: 0001, account: rand(8**8).to_s.rjust(8, '0'), user_id: 1, amount: 1000 },
            { agency: 0001, account: rand(8**8).to_s.rjust(8, '0'), user_id: 2, amount: 2000 },
            { agency: 0001, account: rand(8**8).to_s.rjust(8, '0'), user_id: 3, amount: 3000 },
            { agency: 0001, account: rand(8**8).to_s.rjust(8, '0'), user_id: 3, amount: 4000 }]

User.create(users)
Account.create(accounts)
