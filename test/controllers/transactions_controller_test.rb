require 'test_helper'

class TransactionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    get '/users/sign_in'
    sign_in users(:one)
    @transaction = Transaction.first
  end

  test 'should get new' do
    get new_transaction_url
    assert_response :success
  end

  test 'should create transaction' do
    assert_difference('Transaction.count') do
      post transactions_url, params: { transaction: {
        source_account: accounts(:one), destination_account: accounts(:two).account,
        amount: '20,50'
      } }
    end
    assert_redirected_to transaction_url(Transaction.last)
  end

  test 'should create transaction destination account not found' do
    post transactions_url, params: { transaction: {
      source_account: accounts(:one), destination_account: "2323",
      amount: '20,50'
    } }

    get new_transaction_url
    assert_response :success
  end

  test 'should show transaction' do
    get transaction_url(Transaction.last)
    assert_response :success
  end

  test 'should get users/sign_in' do
    sign_out users(:one)

    get '/users/sign_in'
    assert_response :success
  end
end
