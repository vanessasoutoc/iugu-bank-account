require 'test_helper'

class HomeControllerTest < ActionDispatch::IntegrationTest
  setup do
    get '/users/sign_in'
    sign_in users(:one)
  end

  test 'should get index' do
    get home_index_url
    assert_response :success
  end

  test 'should get users/sign_in' do
    sign_out users(:one)

    get '/users/sign_in'
    assert_response :success
  end
end
