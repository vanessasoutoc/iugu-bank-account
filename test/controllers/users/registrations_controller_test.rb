require 'test_helper'

class RegistrationsControllerTest < ActionDispatch::IntegrationTest
  include Devise::Test::IntegrationHelpers

  test 'should sign up user' do
    assert_difference('User.count') do
      post user_registration_url, params: { user: {
        email: 'loja05@gmail.com', password: 'loja0505'
      } }
    end
    get root_path
    assert_response :success
  end
end
