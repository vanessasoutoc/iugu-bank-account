require 'test_helper'

class TransactionTest < ActiveSupport::TestCase
  def setup
    @transaction = Transaction.new({
                                     source_account: accounts(:one),
                                     destination_account: accounts(:two),
                                     amount: '50,20'
                                   })
  end

  test 'valid transaction' do
    assert @transaction.valid?
  end

  test 'invalid transaction without user' do
    @transaction.source_account_id = nil
    refute @transaction.valid?
    assert_not_nil @transaction.errors[:source_account_id]
  end
end
