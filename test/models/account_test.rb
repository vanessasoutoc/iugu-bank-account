require 'test_helper'

class AccountTest < ActiveSupport::TestCase
  def setup
    # @users = User.new([{ email: 'loja01@gmail.com', password: 'loja0123' },
    #                   { email: 'loja02@gmail.com', password: 'loja0223' }])
    @accounts = Account.new({ account: rand(8**8).to_s.rjust(8, '0'), user_id: 1, agency: 0001, amount: 1000 })
  end

  test 'valid account' do
    assert @accounts.valid?
  end

  test 'invalid account without user' do
    @accounts.user_id = nil
    refute @accounts.valid?
    assert_not_nil @accounts.errors[:user_id]
  end
end
