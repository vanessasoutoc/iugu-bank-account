# README

App created for Iugu.

* Ruby version
2.7.2

* System dependencies
Postgresql
Redis
Unicorn
Nginx

* How to run the test suite

$ RAILS_ENV=test rake test

Use ```rake test```, pois existe um bug já relatado onde o coverage é instável ao usar ```rails test```

* Services (job queues, cache servers, search engines, etc.)

* Deployment DOCKER instructions
Rode os comandos abaixo:

```$ docker build -t rails-toolbox \
       --build-arg USER_ID=$(id -u)  \
       --build-arg GROUP_ID=$(id -g) \
       -f Dockerfile.rails .```

```$ docker volume create --name iugu-postgres```

```$ docker volume create --name iugu-redis```

```$ docker-compose run iugu yarn install --check-files```

```
- OSX/Windows users will want to remove --­­user "$(id -­u):$(id -­g)"
$ docker­-compose run --­­user "$(id ­-u):$(id -­g)" iugu rake db:reset
$ docker­-compose run --­­user "$(id ­-u):$(id -­g)" iugu rake db:migrate
$ docker­-compose run --­­user "$(id ­-u):$(id -­g)" iugu rake db:seed
```

- Run test with Docker
``` $ docker­-compose run iugu rake test```

- Run App
```$ docker-compose up -d```

Acesso na url
http://0.0.0.0:8010/

* Users System

email: 'loja01@gmail.com', password: '@loja0123'
email: 'loja02@gmail.com', password: '@loja0223'
email: 'loja03@gmail.com', password: '@loja0323'
email: 'loja04@gmail.com', password: '@loja0423'
